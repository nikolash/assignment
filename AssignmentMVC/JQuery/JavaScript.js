$(document).ready(function () {
    var count = 1;
    $("#AddContact").click(function () {
        $("#AppendItem").after('<div class="row"><div class="form-group"><div class=" col-sm-3 col-md-4"></div><div class="input-group col-sm-7 col-md-8"><span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span><select class="form-control" id="Contact" name="Contact"><option>ContactType</option><option>Mobile</option><option>Home</option><option>Office</option><option>Fax</option><option>AlternateMobile</option><option>Landline</option></select><input id="ContactNumber' + count + '" name="ContactNumber[]" type="text" value="" placeholder="Contact number." class="form-control"></div><div class="col-sm-offset-4 col-xs"></div></div></div>');
        count++;
    });

    $("#PromoteUser").click(function () {
        var formData = $("#PromoteForm").serialize();
        $.ajax({
            type: "POST",
            url: "/Admin/Promote",
            data: formData,
            success: function (da) {
                if (da.Result == "Success") {
                    alert('User has been Promoted.');
                    location.reload();
                    $("PromoteModal").hide();
                }
                else {

                    alert('Error,Promoting the USER.');
                    location.reload();
                    $("#PromoteModal").hide();
                }
            },
            error: function (da) {
                alert('Error In Promotion.');
                location.reload();
                $("#PromoteModal").hide();
            }
        });
    });

    $("#EditUser").click(function () {
        var formData = $("#EditForm").serialize();
        $.ajax(
        {
            type: "POST", //HTTP POST Method  
            url: "/Edit/Edit", // Controller/View   
            data: formData,
            success: function (da) {
                if (da.Result == "Success") {
                    alert('Saved sucessfully');
                    location.reload();
                    $("#EditModal").hide();
                } else {

                    alert('Error, Fill Data Correctly.');
                    location.reload();
                    $("#EditModal").hide();
                }
            },
            error: function (da) {
                alert('Error In Updation');
            }

        });

    });

    $("#Email").keyup(function () {
        var email = $("#Email").val();
        $.ajax({
            type: "POST",
            url: "/Registration/CheckMail",
            data: '{Email:"' + email + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (result) {
                var message = $("#EmailNotAvailable");
                if (result) {
                    if ($("#Email").val() === "") {
                        message.html("");
                    }
                    else {
                        message.html("Email ID is available.");
                        message.css("color", "green");
                    }
                }
                else {
                    message.html("Email ID is not available.");
                    message.css("color", "red");
                }
            }

        });
    });


    //$("#LoginForm").validate({
    //    rules: {
    //        Email: {
    //            required: true,
    //            email: true
    //        },
    //        Password: {
    //            required: true,
    //            minlength: 8,
    //            passwordPattern: true
    //        }
    //    },
    //    messages: {
    //        Email: {
    //            required: "This field is required !",
    //        },
    //        Password: {
    //            required: "This field is required !"
    //        }
    //    },
    //    errorPlacement: function (error, element) {
    //        error.appendTo(element.parent().siblings().last());
    //    }
    //});




    //$("#RegistrationForm").validate({

    //       rules: {
    //            Firstname: {
    //                required: true,
    //                lettersOnly: true,
    //                maxlength: 25
    //            },
    //            Lastname: {
    //                required: true,
    //                lettersOnly: true,
    //                maxlength: 25
    //            },
    //            Email: {
    //                required: true,
    //                email: true,
    //                maxlength:128
    //            },
    //            Address: {
    //                required: true
    //            },
    //            Password: {
    //                required: true,
    //                minlength: 8,
    //                passwordPattern: true,
    //                maxlength:50
    //            },
    //            ConfirmPassword: {
    //                required: true,
    //                equalTo: "#Password"
    //            }
    //     },
    //       messages: {
    //            firstname: {
    //                required: "This field is required !"
    //            },
    //            Lastname: {
    //                required: "This field is required !"
    //            },
    //            Email: {
    //                required: "This field is required !",
    //            },
    //            Address: {
    //                required: "This field is required !"
    //            },
    //            Password: {
    //                required: "This field is required !"
    //            },
    //            ConfirmPassword: {
    //                required: "This field is required !",
    //                equalTo: "Password does't match !"
    //            }
    //    },
    //       errorPlacement: function (error, element) {
    //         error.appendTo(element.parent().siblings().last());
    //      }
    //    });
    //    $.validator.addMethod("lettersOnly", function (value, element) {
    //        return /^[a-z ]+$/i.test(value);
    //    }, "Only alphabets are allowed for this field !"
    //    );
    //    $.validator.addMethod("passwordPattern", function (value, element) {
    //        return /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/.test(value);
    //    }, "This field will take minimum 8 charectres(Lowercase,Uppercase,Numeric and Special charecters)!"
    //);


    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase(), regex = new RegExp("(.*?)\.(jpg|jpeg|png)$");

        if (!(regex.test(val)) || this.files[0].size >= 512000) {
            $(this).val('');
            alert('File Format/Size is Invalid !');
        }
    });

    $('[id^=ContactNumber]').each(function (e) {
        $(this).rules('add', {
            maxlength: 12,
            number: true
        });
    });
});

function editUserData(id) {
    $("#LoadEditPartial").empty();
    $.ajax({
        type: "POST",
        url: "/Admin/GetUserInfo?id=" + id,
        success: function (result) {
            $("#LoadEditPartial").append('<div class="form-horizontal"><input data-val="true" data-val-number="The field UserId must be a number." data-val-required="The UserId field is required." id="UserId" name="UserId" type="hidden" value="' + result.UserId + '"><div class="form-group"><label class="control-label col-md-2" for="Firstname">Firstname</label><div class="col-md-10"><input class="form-control text-box single-line" id="Firstname" name="Firstname" type="text" value="' + result.Firstname + '"></div></div><div class="form-group"><label class="control-label col-md-2" for="Lastname">Lastname</label><div class="col-md-10"><input class="form-control text-box single-line" id="Lastname" name="Lastname" type="text" value="' + result.Lastname + '"></div></div><div class="form-group"><label class="control-label col-md-2" for="Address">Address</label><div class="col-md-10"><textarea class="form-control" cols="20" id="Address" name="Address" rows="2">' + result.Address + '</textarea></div></div></div>');
        },
        error: function (result) {
            alert('Error In Edit.');
            location.reload();
        }
    });
}