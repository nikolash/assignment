﻿using AssignmentMVC.BAL;
using AssignmentMVC.BusinessClass;
using AssignmentMVC.ViewModel;
using System.Linq;
using System.Web.Mvc;

namespace AssignmentMVC.Controllers
{
    [Authorize]
    
    public class EditController : Controller
    {

        /// <summary>
        /// Edit User Informations
        /// </summary>
        /// <param name="edit"></param>
        /// <returns>JSON</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditModel edit)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Result = "Failuer" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var localEmail = UserCookie.GetCookie(Request); ;
                if (localEmail != string.Empty)
                {
                    BLL reference = new BLL();
                    var localObject = reference.Users.Where(m => m.UserId == edit.UserId).SingleOrDefault();
                    localObject.Address = edit.Address;
                    localObject.Firstname = edit.Firstname;
                    localObject.Lastname = edit.Lastname;
                    reference.SaveChanges();
                    return Json(new { Result = "Success" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { Result = "Failuer" }, JsonRequestBehavior.AllowGet);
                }

            }

        }
    }
}