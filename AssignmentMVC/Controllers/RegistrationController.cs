﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssignmentMVC.BAL;
using AssignmentMVC.ViewModel;
using System.Web.Security;
using System.IO;
using AssignmentMVC.BusinessClass;

namespace AssignmentMVC.Controllers
{
    [Authorize]
    public class RegistrationController : Controller
    {
        BLL Reference = new BLL();
        DAL LogicObject = new DAL();

        /// <summary>
        /// Index Page for Website
        /// </summary>
        /// <returns>View of INDEX</returns>
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("DashBoard", "User");
            }
            return View();
        }


        /// <summary>
        /// GET the registration page Action Method.
        /// </summary>
        /// <returns>View Page for Registration</returns>
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }



        /// <summary>
        /// Post Method for Creating the User and save details in the Database.
        /// </summary>
        /// <param name="registrationModel"></param>
        /// <returns>Dashboard View with Users Object</returns>
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegistrationModel registrationModel)
        {
            if (ModelState.IsValid)
            {
                for (int flag = 0; flag < registrationModel.ContactNumber.Count(); flag++)
                {
                    var checkStatus = BusinessClass.ContactValidation.ValidateContact(registrationModel.ContactNumber[flag]);
                    if (!checkStatus)
                    {
                        ViewBag.Contact = "Invalid Contact field/Fields";
                        return View("Register");
                    }
                }
                var dataObject = Reference.Users.Where(m => m.Email == registrationModel.Email).SingleOrDefault();
                if (dataObject == null)
                {
                    if (registrationModel.ImageURL == null || ((BusinessClass.ImageValidator.ImgaeValidate(Path.GetExtension(registrationModel.ImageURL.FileName))) && registrationModel.ImageURL.ContentLength <= 512000))
                    {
                        string email = LogicObject.Create(registrationModel);
                        if (email != string.Empty)
                        {
                            FormsAuthentication.GetAuthCookie(email, false);
                            return RedirectToAction("DashBoard", "User");
                        }
                        else
                        {
                            return RedirectToAction("Login");
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Invalid Image Format/Size !";
                        return View("Register");
                    }
                }
                else
                {
                    ViewBag.Message = "Email Already Exists !";
                    return View("Register");
                }

            }
            else
            {
                return View("Register");
            }

        }





        /// <summary>
        /// Logout and FormAuthentication is destroyed.
        /// </summary>
        /// <returns>Removes Cookie</returns>
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            this.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.Response.Cache.SetNoStore();
            return RedirectToAction("Login", "Login");
        }


        /// <summary>
        /// Method for fetching information from Database to know Emial is present or not.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult CheckMail(string email)
        {
            var obj = Reference.Users.Where(m => m.Email == email).SingleOrDefault();
            return obj != null ? Json(false) : Json(true);
        }

    }
}