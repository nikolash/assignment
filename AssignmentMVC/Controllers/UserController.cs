﻿using AssignmentMVC.BAL;
using AssignmentMVC.BusinessClass;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace AssignmentMVC.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        /// <summary>
        /// User dashBoard or User details to fetch form the database.
        /// </summary>
        /// <returns></returns>
        public ActionResult DashBoard()
        {
            var localEmial = UserCookie.GetCookie(Request);
            if (localEmial != string.Empty)
            {
                using (BLL Reference = new BLL())
                {
                    var user = Reference.Users.Where(m => m.Email == localEmial).SingleOrDefault();
                    var userRole = Reference.AccessLayers.Where(m => m.UserId == user.UserId).SingleOrDefault();
                    if (userRole.ModuleId == 1)
                    {

                        FormsAuthentication.SetAuthCookie(localEmial, false);
                        return RedirectToAction("Index", "Admin");
                    }
                    else
                    {
                        DAL logicObject = new DAL();
                        var obj = logicObject.ReadData(localEmial);
                        return View(obj);
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }
    }
}