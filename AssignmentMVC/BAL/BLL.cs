﻿using AssignmentMVC.Models;
using System.Data.Entity;

namespace AssignmentMVC.BAL
{

    public class BLL : DbContext
    {
        public BLL() : base("Registration")
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<UserContact> UserContacts { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<AccessLayer> AccessLayers { get; set; }
    }
}