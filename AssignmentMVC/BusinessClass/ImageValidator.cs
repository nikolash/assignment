﻿namespace AssignmentMVC.BusinessClass
{

    //Class for Image Validator
    public class ImageValidator
    {
        public static bool ImgaeValidate(string value)
        {
            string[] extensions = { ".jpg", ".jpeg", ".png", ".JPG", ".JPEG", ",PNG" };
            foreach (var imageValue in extensions)
            {
                if (imageValue.Equals(value))
                {
                    return true;
                }
            }
            return false;
        }
    }
}