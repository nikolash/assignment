﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace AssignmentMVC.BusinessClass
{
    //Validation of Contacts 
    public class ContactValidation
    {
        public static bool ValidateContact(string value)
        {
            if (value == "")
            {
                return true;
            }
            else
            {
                var regex = @"^\d{5,12}$";
                return Regex.IsMatch(value, regex);
            }

        }
    }
}