﻿using System.Security.Cryptography;
using System.Text;
namespace AssignmentMVC.HashValue
{
    //Hash value generator
    public class HashValue
    {
        public static string GetHashCode(string value)
        {
            byte[] byteCode = ASCIIEncoding.ASCII.GetBytes(value);
            byte[] byteHash = new MD5CryptoServiceProvider().ComputeHash(byteCode);
            return (ByteArrayToString(byteHash));
        }

        static string ByteArrayToString(byte[] arrInput)
        {
            int i;
            StringBuilder sOutput = new StringBuilder(arrInput.Length);
            for (i = 0; i < arrInput.Length; i++)
            {
                sOutput.Append(arrInput[i].ToString("X2"));
            }
            return sOutput.ToString();
        }
    }
}