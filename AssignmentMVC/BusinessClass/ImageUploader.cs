﻿using System.Web.Mvc;

namespace AssignmentMVC.BusinessClass
{
    //Helper for Image Uploader
    public static class ImageUploader
    {
        public static MvcHtmlString Image(this HtmlHelper helper, string src, string altText, string height)

        {

            var builder = new TagBuilder("img");

            builder.MergeAttribute("src", src);

            builder.MergeAttribute("alt", altText);

            builder.MergeAttribute("height", height);
            builder.MergeAttribute("class", "image-view");

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString FileFor(this HtmlHelper helper, string Id, string File)
        {

            var builder = new TagBuilder("input");

            builder.MergeAttribute("type", "file");

            builder.MergeAttribute("Id", Id);

            builder.MergeAttribute("name", File);

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));

        }

    }
}