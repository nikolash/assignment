﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssignmentMVC.ViewModel
{
    public class Promote
    {
        public int UserId { get; set; }

        public int PromoteId { get; set; }
    }
}