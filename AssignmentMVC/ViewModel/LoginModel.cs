﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssignmentMVC.ViewModel
{
    public class LoginModel
    {
        [Required(ErrorMessage = "This field is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Password { get; set; }
    }
}


/*
 * @model AssignmentMVC.ViewModel.RegistrationModel
@using AssignmentMVC.ViewModel
@using AssignmentMVC.BusinessClass


@{
    ViewBag.Title = "Register";
    Layout = "~/Views/Shared/_Layout.cshtml";
}

<div>
    @using (Html.BeginForm("Register", "Registration",FormMethod.Post, new { enctype = "multipart/form-data" }))
    {
        @Html.AntiForgeryToken()
        @Html.ValidationSummary(true)
        <div>
            <h3>Regitration</h3>
        
        </div>
        <div>
            @Html.LabelFor(m=>m.Firstname)
            @Html.TextBoxFor(m=>m.Firstname)
            @Html.ValidationMessageFor(m=>m.Firstname)
        </div>

        <div>
            @Html.LabelFor(m => m.Lastname)
            @Html.TextBoxFor(m => m.Lastname)
            @Html.ValidationMessageFor(m => m.Lastname)
        </div>

        <div>
            @Html.LabelFor(m => m.Email)
            @Html.TextBoxFor(m => m.Email)
            @Html.ValidationMessageFor(m => m.Email)
        </div>

        <div>
            @Html.LabelFor(m => m.Address)
            @Html.TextAreaFor(m => m.Address)
            @Html.ValidationMessageFor(m => m.Address)
        </div>
        <div>
            @Html.Label("Contacts")
        </div>
        <div id="CloneItem">
            @Html.DropDownListFor(m => m.Contact, new SelectList(Enum.GetValues(typeof(Contact))))
        </div>
        <div>
            @Html.TextBoxFor(m => m.ContactNumber)
        </div>
        <div id="AppendItem">
            @Html.Label("More Contact")
            <button type="button" id="AddContact">Add</button>
        </div>
        <div>
            @Html.LabelFor(m=>m.ImageURL,"Upload Image")
            @Html.FileFor("ImageURL","ImageURL")
        </div>


        <div>
            @Html.LabelFor(m => m.Password)
            @Html.PasswordFor(m => m.Password)
            @Html.ValidationMessageFor(m => m.Password)
        </div>

        <div>
            @Html.LabelFor(m => m.ConfirmPassword)
            @Html.PasswordFor(m => m.ConfirmPassword)
            @Html.ValidationMessageFor(m => m.ConfirmPassword)
        </div>

        <div>
            <input type="submit" value="Register" />
        </div>
    }
</div>

    */