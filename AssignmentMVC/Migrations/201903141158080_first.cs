namespace AssignmentMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Registration.AccessLayers",
                c => new
                    {
                        AccessId = c.Int(nullable: false, identity: true),
                        ModuleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccessId)
                .ForeignKey("Registration.Modules", t => t.ModuleId, cascadeDelete: true)
                .ForeignKey("Registration.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ModuleId)
                .Index(t => t.UserId);
            
            CreateTable(
                "Registration.Modules",
                c => new
                    {
                        ModuleId = c.Int(nullable: false, identity: true),
                        ModuleName = c.String(maxLength: 25),
                    })
                .PrimaryKey(t => t.ModuleId);
            
            CreateTable(
                "Registration.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Firstname = c.String(nullable: false, maxLength: 25),
                        Lastname = c.String(nullable: false, maxLength: 25),
                        Email = c.String(nullable: false, maxLength: 128),
                        Password = c.String(nullable: false, maxLength: 50),
                        Address = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "Registration.Contacts",
                c => new
                    {
                        ContactId = c.Int(nullable: false, identity: true),
                        ContactType = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.ContactId);
            
            CreateTable(
                "Registration.Images",
                c => new
                    {
                        ImageId = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 25),
                        ImageURL = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ImageId)
                .ForeignKey("Registration.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Registration.UserContacts",
                c => new
                    {
                        UserContactId = c.Int(nullable: false, identity: true),
                        ContactNumber = c.String(maxLength: 12),
                        UserId = c.Int(nullable: false),
                        ContactId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserContactId)
                .ForeignKey("Registration.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("Registration.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ContactId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Registration.UserContacts", "UserId", "Registration.Users");
            DropForeignKey("Registration.UserContacts", "ContactId", "Registration.Contacts");
            DropForeignKey("Registration.Images", "UserId", "Registration.Users");
            DropForeignKey("Registration.AccessLayers", "UserId", "Registration.Users");
            DropForeignKey("Registration.AccessLayers", "ModuleId", "Registration.Modules");
            DropIndex("Registration.UserContacts", new[] { "ContactId" });
            DropIndex("Registration.UserContacts", new[] { "UserId" });
            DropIndex("Registration.Images", new[] { "UserId" });
            DropIndex("Registration.AccessLayers", new[] { "UserId" });
            DropIndex("Registration.AccessLayers", new[] { "ModuleId" });
            DropTable("Registration.UserContacts");
            DropTable("Registration.Images");
            DropTable("Registration.Contacts");
            DropTable("Registration.Users");
            DropTable("Registration.Modules");
            DropTable("Registration.AccessLayers");
        }
    }
}
