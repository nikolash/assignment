﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssignmentMVC.Models
{
    [Table("Modules", Schema = "Registration")]
    public class Module
    {
        [Key]
        public int ModuleId { get; set; }

        [MaxLength(25)]
        public string ModuleName { get; set; }
    }
}