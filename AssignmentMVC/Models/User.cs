﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AssignmentMVC.Models
{
    [Table("Users", Schema = "Registration")]
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [MaxLength(25)]
        public string Firstname { get; set; }
        [Required]
        [MaxLength(25)]
        public string Lastname { get; set; }

        [MaxLength(128)]
        [Required]
        public string Email { get; set; }
        [Required]
        [MaxLength(50)]
        public string Password { get; set; }

        [Required]
        public string Address { get; set; }
    }
}